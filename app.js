
import React from 'react';
import NoteApp from './components/note/NoteApp';
import NoteShare from './components/note/NoteShare';

import NoteNew from './components/note/NoteNew';
import NoteShow from './components/note/NoteShow';
import NoteEdit from './components/note/NoteEdit';

import {default as Router, Route} from 'react-router';

let RouterHandler = Router.RouteHandler;

class AppRouter extends React.Component {
   onSubmit(e){
      e.preventDefault();
      var $data = $("#login-form").serialize();                
      $.ajax({
         method: "POST",
         url: '/login',
         dataType: "json",
         data: $data,
         cache: false,
         timeout: 2000,
         success: function(data) {
            if(data.login){
               $("#login-div").fadeOut(1000, function() {
               });
               $("#user-span").text(data.username);
               $.notify("Login Exitoso", {
                  className: "success",
                  autoHideDelay: 2200,
                  position:"bottom right"
               });
            }
         },
         error: function(jqXHR, textStatus, errorThrown) {
            $.notify("Login Incorrecto", {
               className: "error",
               autoHideDelay: 2500,
               position:"bottom right"
               });
            }
         });
   }

   render() {
      /*
      <li className="liToggle hidden-tag">
               <a href="#settings">
                  <i className="fa fa-cog fa-3x"></i>
                  Ajustes
               </a>
            </li>
            <li className="liToggle hidden-tag">
               <a href="#administrator">
                  <i className="fa fa-user fa-3x"></i>
                  Administrador
               </a>
            </li>
      */
      return <div>
      <nav className="navbar-default navbar-side" role="navigation">
      <div className="sidebar-collapse">
         <ul className="nav" id="main-menu">
            <li className="liToggle">
               <a href="#mynotes">
                  <i className="fa fa-list-alt fa-3x"></i>
                  Mis Notas
               </a>
            </li>
            <li className="liToggle">
               <a href="#newnote">
                  <i className="fa fa-edit fa-3x"></i>
                  Nueva Nota
               </a>
            </li>
            <li className="liToggle">
               <a href="#sharenotes">
                  <i className="fa fa-share-square fa-3x"></i>
                  Notas Compartidas
               </a>
            </li>
            <li>
               <a href="/logout">
                  <i className="fa fa-sign-out fa-3x"></i>
                  Cerrar Sesion
               </a>
            </li>
            <li className="liToggle">
               <a href="#about">
                  <i className="fa fa-info-circle fa-3x"></i>
                  Acerca de Free Notes
               </a>
            </li>
         </ul>
      </div>
      </nav>
         <div id="page-wrapper" >
            <div id="page-inner">
               <div id="login-div" className="panel panel-success" >
               <div className="panel-heading">
               <span>{"INICIO DE SESION"}</span>
               <div className="panel-body">
                  <div className="row">
                     <div className="col-md-12">
                        <form action="/login" method="post" id="login-form" onSubmit={this.onSubmit.bind(this)}>
                           <div className="form-group has-success">
                              <label className="control-label">{"Username:"}</label>
                              <input type="text" className="form-control" name="username" />
                           </div>
                           <div className="form-group has-success">
                              <label className="control-label">{"Password:"}</label>
                              <input type="password" className="form-control" name="password" />
                           </div>
                           <div className="form-group has-success">
                              <button type="button" className="btn btn-success" data-toggle="modal" data-target="#myModal">
                                 {"Registrarse"}
                              </button>
                              <button type="submit" value="Log In" className="btn btn-primary pull-right">
                                 {"Login"}
                              </button>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
               </div>
               </div>
               <div id="page-inner-detail">
                  <RouterHandler />
               </div>
               <input type="hidden" id="input-share"/>
            </div>
         </div>
      </div>
   }
}

let routes = <Route handler={AppRouter}>
      <Route path="mynotes" handler={NoteApp} />
      <Route path="newnote" handler={NoteNew} />
      <Route path="shownote/:noteID" handler={NoteShow} />
      <Route path="editnote/:noteID" handler={NoteEdit} />
      <Route path="sharenotes" handler={NoteShare} />
      <Route path="settings" handler={NoteNew} />
      <Route path="administrator" handler={NoteNew} />
      <Route path="about" handler={NoteNew} />
   </Route>

Router.run(routes, Router.HashLocation, (Root) => {
   React.render(<Root />, document.getElementById('wrapper-detail'));
   
   if($("#user-span").text() !== ""){
      $("#login-div").hide();
   }
});

/*$(window).bind('beforeunload', function(){
   return 'Guarden los datos antes de continuar, de lo contrario perderán los cambios';
});*/
$(document).ready(function() {
   $(".liToggle").on("click", function(){
      if ($(window).width() < 768) {
         $(".navbar-toggle").click();
      }
   });

   $('#modalShare').on('hidden.bs.modal', function () {
      $("#user-list").html("");
   })

   $("#register-user").on("click", function(e){
      e.preventDefault();
      let $data = {
         username: $("#username").val(),
         password: $("#password").val(),
         confirm: $("#confirm").val()
      }
      $.ajax({
         method: "POST",
         url: '/register',
         dataType: "json",
         data: $data,
         cache: false,
         timeout: 2000,
         success: function(data) {
            if(data.save){
               $.notify(data.text, {
                  className: "success",
                  autoHideDelay: 2200,
                  position:"bottom right"
               });
            }
         },
         error: function(jqXHR, textStatus, errorThrown) {
            let $json = $.parseJSON(jqXHR.responseText);
            $.notify($json.text, {
               className: "error",
               autoHideDelay: 2500,
               position:"bottom right"
               });
            }
      });
   });

   $("#share-note").on("click", function(e){
      e.preventDefault();
      let shares = $('input[type=checkbox]:checked').map(function(_, el) { return {user_id: $(el).val()} }).get();
      console.log(shares);
      let $data = {
         note_id: window.location.hash.substring(11),
         shares: shares
      }
      $.ajax({
         method: "POST",
         url: '/noteshare/'+window.location.hash.substring(11),
         dataType: "json",
         data: $data,
         cache: false,
         timeout: 2000,
         success: function(data) {
            if(data.save){
               $.notify(data.text, {
                  className: "success",
                  autoHideDelay: 2200,
                  position:"bottom right"
               });
            }
         },
         error: function(jqXHR, textStatus, errorThrown) {
            let $json = $.parseJSON(jqXHR.responseText);
            $.notify($json.text, {
               className: "error",
               autoHideDelay: 2500,
               position:"bottom right"
               });
            }
      });
   });
});