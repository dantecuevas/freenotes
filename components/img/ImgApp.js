import React from 'react';

class ImgApp extends React.Component {
   constructor(props) {
      super(props);
      this.state = {msg: []};
   }

   render() {
      return <div>
         {
            this.props.images.map((image) => {
               let key = image._id || image.uid;
               let url = `uploads/thumbnails/medium-${image.img}`;
               let divStyle = {
                  backgroundImage: 'url('+url+')',
                  backgroundRepeat: 'no-repeat',
                  margin: "0 auto",
               };
               return <div key={key} className="text-center">
                     <div className="divMediumImg">
                        <div style={{position: 'absolute', marginTop: '-10px', marginLeft: '-10px'}}>
                           <button type="button" className="btn btn-default btn-xs">
                              <i className="fa fa-times-circle fa-2x"></i>
                           </button>
                        </div>
                        <div style={divStyle} className="divMediumImg">
                        </div>
                     </div>
                     <input type="text" className="form-control images-msg" defaultValue={image.msg} />
                  <br/>
               </div>
            })
         }
      </div>
   }
}

export default ImgApp