
import React from 'react';
import NoteTable from './NoteTable';

class NoteApp extends React.Component {
   constructor(props) {
      super(props);
      this.state = {notes: [], noteState: false, share: false};
   }

   componentWillMount() {
      if(!this.state.noteState) {
         $.get('/notes', (notes) => {
            this.setState({notes: notes, noteState: true});
         });
      }      
   }

   onClick () {
      window.location = "/#/newnote";
   }

   componentDidMount() {
      //
   }

   render() {
      if(this.state.notes.length) {
         if(this.state.notes[0].noData){
            return <p> No tienes notas </p>
         }
         return <div className="row" id="pokeapp-id">
            <NoteTable notes={this.state.notes} share={this.state.share}/>
            <div className="flotante1">
               <button type="button" className="btn btn-success btn-circle" onClick={this.onClick.bind(this)}>
                  <i className="fa fa-plus fa-2x"></i>
               </button>
            </div>
         </div>
      } else {
         return <p> Cargando... </p>
      }
   }

}

export default NoteApp