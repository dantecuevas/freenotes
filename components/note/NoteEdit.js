
import React from 'react';
import ImgApp from '../img/ImgApp';
import uid from 'uid';

class NoteEdit extends React.Component {
   constructor(props) {
      super(props);
      this.state = {note: [], show: false, images: []};
   }

   componentWillMount() {
      if(!this.state.show) {
         let hash = (window.document.location.hash).split("/");
         $.get('/note/' + hash[2], (note) => {
            this.setState({note: note, show: true, images: note.images});
         });
      }
   }

   componentDidUpdate() {
      if(this.state.note.images == this.state.images){
         $("#note-title").val(this.state.note.title);
         $("#note-mensaje").val(this.state.note.mensaje);
      }
   }

   imagesMsg(stateImages) {
      let $imagesFull = stateImages;
      $(".images-msg").each(function(i, e) {
         $imagesFull[i].msg = $(e).val();
      });
      return $imagesFull;
   }

   onClick(ev){
      ev.preventDefault();
      let $images = this.imagesMsg(this.state.images);
      
      let $data = {
         title: $("#note-title").val(),
         mensaje: $("#note-mensaje").val(),
         images: $images
      };
      let id = window.location.hash.substring(11);
      $.ajax({
         method: "POST",
         url: `/note/${id}`,
         dataType: "json",
         data: $data,
         cache: false,
         timeout: 2000,
         success: function(data) {
            if(data.save){
               $.notify("Nota Actualizada Exitosamente", {
                  className: "success",
                  autoHideDelay: 2200,
                  position:"bottom right"
               });
               window.location = "/#/mynotes";
            }
            $(':input', "#note-new-form")
            .not(':button, :submit, :reset, :hidden')
            .val('');
         },
         error: function(jqXHR, textStatus, errorThrown) {
            var $json = $.parseJSON(jqXHR.responseText);
            
            if($json.save==false){
               $.notify("Error al guardar", {
                  className: "error",
                  autoHideDelay: 2500,
                  position:"bottom right"
               });
            }
            if($json.login==false){
               $.notify("Debes Iniciar Sesion", {
                  className: "info",
                  autoHideDelay: 2500,
                  position:"bottom right"
               });
            }
         }
      });
   }

   onImage(ev){
      ev.preventDefault();
      $("#note-img-input").click();
   }

   onCancel(ev){
      console.log("CANCEL");
   }

   change(ev) {
      let $self = this;
      if($("#note-img-input").val() !== ''){
         $("#note-loading-div").show();

         /*var Archivos,Lector,origen;
         Archivos = $('#note-img-input')[0].files;
         if(Archivos.length>0){
            Lector = new FileReader();
            Lector.onloadend = function(e){
               origen = e.target;
            };
            Lector.onerror = function(e) {
               console.log(e);
            };
            Lector.readAsDataURL(Archivos[0]); 
         }*/

         $("#uploadForm").ajaxSubmit({
            beforeSend: function() {
               $('#progress-bar-div-id').css({"width": "0%"});
               $('#progress-bar-span-id').html('0%');
            },
            uploadProgress: function(event, position, total, percentComplete) {
               let percent = percentComplete + '%';
               $('#progress-bar-div-id').css({"width": percent});
               $('#progress-bar-span-id').html(percent);
            },
            error: function(xhr) {
               console.log('Error: ' + xhr.responseText);
            },
            success: function(response) {
               var $data = $.parseJSON(response);
               //console.log($self.state.images);
               let images = $self.state.images;
               images.push({img: $data.imgName, msg: "", uid: uid()});
               images = $self.imagesMsg(images);
               $self.setState({images: images});
               $('#note-loading-div').hide("slow");

            }
         });
      }
   }
   render() {
      return <div className="panel panel-success">
            <div className="panel-heading">
               <span>{"NUEVA NOTA"}</span>
               <div className="panel-body">
                  <div className="row">
                     <div className="col-md-12">
                        <div className="hidden-tag">
                           <form id="uploadForm" encType="multipart/form-data" action="/api/noteimg" method="post">
                              <input type="file" id="note-img-input" name="noteImage" onChange={this.change.bind(this)}/>
                           </form>
                        </div>
                        <form id="note-new-form">
                           <div className="form-group has-success">
                              <label className="control-label">{"Titulossss"}</label>
                              <input type="text" className="form-control" name="title" id="note-title" defaultValue={this.state.note.title}/>
                           </div>
                           <div className="form-group has-success">
                              <label className="control-label">{"Mensaje"}</label>
                              <input type="text" className="form-control" name="mensaje" id="note-mensaje" defaultValue={this.state.note.mensaje}/>
                           </div>
                           <div className="form-group has-success">
                              <div className="flotante1">
                                 <button type="button" className="btn btn-primary btn-circle" onClick={this.onImage.bind(this)}>
                                    <i className="fa fa-camera fa-2x"></i>
                                 </button>
                              </div>
                              <div className="flotante2">
                                 <button type="button" className="btn btn-success btn-circle" onClick={this.onClick.bind(this)}>
                                    <i className="fa fa-check fa-2x"></i>
                                 </button>
                              </div>
                              <div className="flotante3 hidden-tag">
                                 <button type="button" className="btn btn-danger btn-circle" onClick={this.onCancel.bind(this)}>
                                    <i className="fa fa-ban fa-2x"></i>
                                 </button>
                              </div>
                           </div>
                           <br/>
                           <div id="note-loading-div"className="form-group text-center hidden-tag">
                              <label className="control-label" style={{width: "100%"}}>
                                 <div className="progress">
                                    <div id="progress-bar-div-id" className="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style={{width: "0%"}}>
                                       <span id="progress-bar-span-id">0%</span>
                                    </div>
                                 </div>
                              </label>
                           </div>
                           <div id="ImgAppRender">
                              <ImgApp images={this.state.images} />
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
      </div>
   }
}

export default NoteEdit