import React from 'react';

class NoteImg extends React.Component {
   imageExist() {
      let url = `uploads/thumbnails/medium-${this.props.img}`;
      let urlFull = `uploads/${this.props.img}`;
      let divStyle = {
         backgroundImage: 'url('+url+')',
         backgroundRepeat: 'no-repeat',
         margin: "0 auto",
      };

      if(this.props.img !== "noImage"){
         return <a className="fancybox" href={urlFull} data-fancybox-group="gallery" title="">
            <div style={divStyle} className="divMediumImg">
            </div>
         </a>
      }
   }

   render() {

      return <div className="form-group text-center">
         <label className="control-label">
            {this.imageExist()}
         </label>
         <br/>
         <label className="control-label">
            {this.props.msg}
         </label>
      </div>
   }
}

export default NoteImg