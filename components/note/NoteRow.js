
import React from 'react';
import NoteView from './NoteView';

class NoteRow extends React.Component {
   onClick(note_id, ev) {
      if(confirm("Desea eleminar esta nota")){
         $.get('/note/delete/'+note_id, (result) => {
            $("#note_id_"+note_id).hide("slow");
         });
      }
   }

   onShare(share, ev) {
      $("#input-share").val(share);
   }

   btnClose() {
      if (!this.props.share) {
         return <div style={{position: 'absolute', marginTop: '-10px', marginLeft: '-10px'}}>
            <button type="button" className="btn btn-default btn-circle-sm" onClick={this.onClick.bind(this, this.props.note_id)}>
               <i className="fa fa-times-circle fa-2x"></i>
            </button>
         </div>
      }
   }

   render() {
      return <div className={"col-md-4 col-sm-4"} id={"note_id_"+this.props.note_id}>
         {this.btnClose()}
         <a href={"#shownote/"+this.props.note_id} onClick={this.onShare.bind(this, this.props.share)}>
         <div className="panel panel-success">
            <NoteView img={this.props.img} title={this.props.title} />
         </div>
         </a>
      </div>
   }
}

class NoteTest extends React.Component {
   render() {
      return <p>View Nota</p>
   }
}

export default NoteRow
