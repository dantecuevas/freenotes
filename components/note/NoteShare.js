
import React from 'react';
import NoteTable from './NoteTable';

class NoteShare extends React.Component {
   constructor(props) {
      super(props);
      this.state = {notes: [], noteState: false, share: true};
   }

   componentWillMount() {
      if(!this.state.noteState) {
         $.get('/shares', (notes) => {
            this.setState({notes: notes, noteState: true});
         });
      }      
   }

   componentDidMount() {
      //
   }

   render() {
      if(this.state.notes.length) {
         if(this.state.notes[0].noData){
            return <p> No tienes notas </p>
         }
         return <div className="row" id="pokeapp-id">
            <NoteTable notes={this.state.notes} share={this.state.share}/>
         </div>
      } else {
         return <p> Cargando... </p>
      }
   }

}

export default NoteShare