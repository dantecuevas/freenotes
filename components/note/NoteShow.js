import React from 'react';
import NoteImg from './NoteImg';
import UserList from '../user/UserList';

class NoteShow extends React.Component {
   constructor(props) {
      super(props);
      this.state = {note: [], show: false};
   }

   componentWillMount() {
      if(!this.state.show) {
         let hash = (window.document.location.hash).split("/");
         let s = "";
         if($("#input-share").val() == "true"){
            s = "share";
         }
         $.get(`/note${s}/` + hash[2], (note) => {
            this.setState({note: note, show: true});
         });
      }
   }

   onClick() {
      window.location = "/#/mynotes";
      //window.history.back();
   }

   onEdit() {
      let hash = (window.document.location.hash).split("/");
      window.location = "/#/editnote/"+hash[2];
      //window.history.back();
   }

   onShare() {
      React.render(<UserList />, document.getElementById('user-list'));
   }

   btnAction() {
      if($("#input-share").val() == "false") {
         return <div>
            <div className="flotante1">
               <button type="button" className="btn btn-success btn-circle" onClick={this.onClick.bind(this)}>
                  <i className="fa fa-chevron-left fa-2x"></i>
               </button>
            </div>
            <div className="flotante2">
               <button type="button" className="btn btn-warning btn-circle" onClick={this.onEdit.bind(this)}>
                  <i className="fa fa-pencil fa-2x"></i>
               </button>
            </div>
            <div className="flotante3">
               <button type="button" className="btn btn-primary btn-circle" data-toggle="modal" data-target="#modalShare" onClick={this.onShare.bind(this)} >
                  <i className="fa fa-share-square fa-2x"></i>
               </button>
            </div>
         </div>
      }
   }

   render() {
      if(!$.isEmptyObject(this.state.note)) {
         if(this.state.note.noData){
            return <p> No existe esta la nota </p>
         }
         return <div className="row">
         <div className="col-md-12">
            <div className="panel panel-info">
               <div className="panel-heading">
               <span>{"MOSTRAR NOTA"}</span>
               <div className="panel-body">
                  <div className="row">
                     <div className="col-md-6">
                        <div className="form-group has-info">
                           <label className="control-label">{"Titulo: "}</label>
                           <br/>
                           {this.state.note.title}
                        </div>
                        <div className="form-group has-info">
                           <label className="control-label">{"Mensaje: "}</label>
                           <br/>
                           {this.state.note.mensaje}
                        </div>
                        {
                           this.state.note.images.map((image) => {
                              return <NoteImg 
                                 key={image._id}
                                 msg={image.msg}
                                 img={image.img} />
                           })
                        }
                     </div>
                  </div>
               </div>
               </div>
            </div>
         </div>
         {this.btnAction()}
         </div>
      } else {
         return <p> Cargando Nota...
            <input type="hidden" id="note_id"/>
         </p>
      }
   }

}

export default NoteShow
