
import React from 'react';
import NoteRow from './NoteRow';

class NoteTable extends React.Component {
   render() {
      return <div className="test">
      {
         this.props.notes.map((note) => {
            let img = "noImage";
            if(note.images.length){
               img = note.images[0].img
            }
            return <NoteRow 
               key={note._id}
               title={note.title}
               img={img}
               note_id={note._id}
               share={this.props.share} />
         })
      }
      </div>
      
   }
}

export default NoteTable