
import React from 'react';

class NoteView extends React.Component {

   imageExist() {
      let url = `uploads/thumbnails/small-${this.props.img}`;

      if(this.props.img !== "noImage"){
         return <img src={url} draggable="false"/>
      }
   }

   render() {
      return <div className="panel-heading min-max-height">
         <div className="right">
            {this.imageExist()}         
         </div>
         <div className="left">
            {this.props.title}
         </div>
      </div>
   }
}

export default NoteView
