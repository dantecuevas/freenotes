
import React from 'react';

class InputCheck extends React.Component {
   constructor(props) {
      super(props);
      this.state = {isChecked: this.props.isChecked};
   }

   onChange() {
      this.setState({isChecked: !this.state.isChecked});
   }

   render() {
      return <input type="checkbox" name="users[]" checked={this.state.isChecked} onChange={this.onChange.bind(this)} defaultValue={this.props.userId}/>
   }
}

export default InputCheck