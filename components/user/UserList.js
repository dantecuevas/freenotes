
import React from 'react';
import UserTable from './UserTable';

class UserList extends React.Component {
   constructor(props) {
      super(props);
      this.state = {users: [], shares: [], userState: false};
   }

   componentWillMount() {
      //if(!this.state.userState) {
         $.get('/usersnote/'+window.location.hash.substring(11), (data) => {
            this.setState({users: data.users, shares: data.shares, userState: true});
         });
      //}
      console.log("USERLIST");      
   }

   render() {
      if(this.state.users.length) {
         if(this.state.users[0].noData){
            return <p> No hay usuarios </p>
         }
         return <UserTable users={this.state.users} shares={this.state.shares}/>
      } else {
         return <div>
            <img src={"img/loading.gif"} width={"200px"} className="center-block"/>
         </div>
      }
   }

}

export default UserList