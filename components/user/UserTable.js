
import React from 'react';
import InputCheck from './InputCheck';

class UserTable extends React.Component {

   render() {
      let note_id = window.location.hash.substring(11);
      let isChecked;
      return <div>
      {
         this.props.users.map((user) => {
            isChecked = false;
            if(this.props.shares) {
               $.each(this.props.shares, function( index, value ) {
                  if(value.user_id == user._id){
                     isChecked = true;
                  }
               });
            }
            return <div key={user._id}>
               {user.username}
               <InputCheck userId={user._id} isChecked={isChecked}/>
                  
            </div>
         })
      }
      </div>
   }
}

export default UserTable