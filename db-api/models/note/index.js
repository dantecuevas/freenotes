
import mongoose from 'mongoose';

let NoteSchema = new mongoose.Schema({
   user_id: {type: String, require: true},
   title: {type: String, require: true},
   mensaje: {type: String, require: true},
   images: [
      {img: String, msg: String}
   ],
   shares: [
      {user_id: String}
   ]
});

export default mongoose.model('Note', NoteSchema)