
import mongoose from 'mongoose';

let ShareSchema = new mongoose.Schema({
   note_id: {type: String, require: true, unique: true},
   users: [
      {user_id: String}
   ]
});

export default mongoose.model('Share', ShareSchema)