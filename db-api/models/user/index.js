
import mongoose from 'mongoose';

let UserSchema = new mongoose.Schema({
   username: {type: String, require: true, unique: true},
   password: {type: String, require: true}
});

export default mongoose.model('User', UserSchema)