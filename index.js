
import express from 'express';
import http from 'http';
import fs from 'fs';

import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import expressSession from 'express-session';
import passport from 'passport';
import multer from 'multer';
import lwip from 'lwip';

import mongoose from 'mongoose';
import User from './db-api/models/user';
import Note from './db-api/models/note';

import p from './configs/params';
   let params = {
      dir: `./${p.img.dir}`,
      thb: p.img.thb,
      n1: p.img.type.m.name,
      s1: p.img.type.m.size,
      n2: p.img.type.s.name,
      s2: p.img.type.s.size,
   };
//mongoose.connect('mongodb://localhost/freenotes');
mongoose.connect('mongodb://freenotes:freenotes@ds019746.mlab.com:19746/freenotes');
import {Strategy as LocalStrategy} from 'passport-local';

//const LocalStrategy = require('passport-local').Strategy;

const port = process.env.PORT || 3001;
const app = express();

let storage = multer.diskStorage({
	destination: function(req, file, callback) {
      callback(null, './assets/uploads');
   },
   filename: function(req, file, callback){
      var basename = file.originalname.split(/[\\/]/).pop(),
      pos = basename.lastIndexOf(".");
      if (basename === "" || pos < 1)
         return "";
      callback(null, file.fieldname + '-' + Date.now() + '.' + basename.slice(pos + 1));
   }
});
let upload = multer({storage: storage}).single("noteImage");

/*view engine setup*/
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(expressSession({
   secret: 'SECRET-KEY',
   resave: false,
   saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());

app.use('/', express.static(__dirname + '/assets'));

let localStrategy = new LocalStrategy( (username, password, done) => {
   //Logica de BASE DE DATOS
   console.log("TEST LocalStrategy");
   User.findOne({username: username, password: password}, (err, docs) => {
      console.log("TEST LocalStrategy FIND");
      if(err) {
         done(null, false, {
            message: 'Error'
         });
      }
      if(docs) {
         return done(null, {
            _id: docs._id,
            username: docs.username
         });
      }else {
         done(null, false, {
            message: 'Unkown user'
         });
      }      
   });
   
});
passport.use(localStrategy);
passport.serializeUser((user, done) => done(null, user));
passport.deserializeUser((user, done) => done(null, user));

app.post('/api/noteimg',ensureAuth ,function(req, res){
   upload(req , res , function(err) {
      if(!err){
         lwip.open(`${params.dir}/${req.file.filename}`, function(err2, image) {
            if(err2) throw err2;
            let imageRatio = 1;
            if(image.width() >= image.height()){
               imageRatio = params.s1/image.height();
            } else {
               imageRatio = params.s1/image.width();
            }
            image.scale(imageRatio, function(err3,rzdImg) {
               rzdImg.writeFile(`${params.dir}/${params.thb}/${params.n1}-${req.file.filename}`, function(err4){
                  if(err4) throw err4;
                  image.resize(params.s2, params.s2, function(err5,rzdImg) {
                     rzdImg.writeFile(`${params.dir}/${params.thb}/${params.n2}-${req.file.filename}`, function(err6){
                        if(err6) throw err6;
                        return res.end(`{"upload": true, "imgName": "${req.file.filename}"}`);
                     });
                  });
               });
            });            
         });
      } else {
         return res.end(`{"upload": false}`);
      }
   });
});

app.post('/note',ensureAuth , (req, res) => {
   let note = new Note({
      user_id: req.user._id,
      title: req.body.title,
      mensaje: req.body.mensaje,
      images: req.body.images
   });
   note.save((err, resp) => {
      if(err){
         console.log(err);
         res.status(401).send({"save": false});
      } else {
         console.log(note);   
         res.send({"save": true, "title": note.title});
      }            
   });
});

app.post('/note/:noteID',ensureAuth , (req, res) => {
   let note = {
      title: req.body.title,
      mensaje: req.body.mensaje,
      images: req.body.images
   };

   Note.findOneAndUpdate({_id:req.params.noteID, user_id: req.user._id}, note, function (err, resp) {
      if(err){
         res.status(401).send({"save": false});
      } else {
         res.send({"save": true, "title": note.title});
      } 
   });
});

app.get('/notes',ensureAuth , (req, res) => {
   Note.find({user_id: req.user._id}, (err, docs) => {

      if(err) {
         res.json();
      }
      if(docs) {
         if(!docs.length){
            docs.push({noData: true});   
         }
         res.json(docs);
      }else {
         res.json({});
      }
   });
   /*dbapi.notes.find((notes) => {
      res.json(notes);
   });*/
});

app.get('/note/delete/:noteID',ensureAuth , (req, res) => {
   Note.findOne({user_id: req.user._id, _id: req.params.noteID}, (err, docs) => {
      if(docs) {
         docs.images.forEach(function (item) {
            fs.unlink(`${params.dir}/${item.img}`);
            fs.unlink(`${params.dir}/${params.thb}/${params.n1}-${item.img}`);
            fs.unlink(`${params.dir}/${params.thb}/${params.n2}-${item.img}`);
         });
      }
      Note.remove({user_id: req.user._id, _id: req.params.noteID}, (err, result) => {

         if(err) {
            res.json();
         }
         if(result) {
            /*if(!result.length){
               result.push({noData: true});   
            }*/
            res.json(result);
         }else {
            res.json({});
         }
      });
   });
});

app.get('/note/:noteID',ensureAuth , (req, res) => {
   Note.findOne({user_id: req.user._id, _id: req.params.noteID}, (err, docs) => {
      if(err) {
         res.json();
      }
      if(docs) {
         if(!docs.length){
            //docs.push({noData: true});
         }
         res.json(docs);
      }else {
         res.json({});
      }
   });
   /*dbapi.notes.find((notes) => {
      res.json(notes);
   });*/
});

app.get('/noteshare/:noteID',ensureAuth , (req, res) => {
   Note.findOne({_id: req.params.noteID}, (err, docs) => {
      if(err) {
         res.json();
      }
      if(docs) {
         if(!docs.length){
            //docs.push({noData: true});
         }
         res.json(docs);
      }else {
         res.json({});
      }
   });
   /*dbapi.notes.find((notes) => {
      res.json(notes);
   });*/
});

app.post('/noteshare/:noteID',ensureAuth , (req, res) => {
   let note = {
      shares: req.body.shares
   };
   Note.findOneAndUpdate({_id: req.params.noteID, user_id: req.user._id}, note, function (err, resp) {
      if(err){
         res.status(401).send({"save": false});
      } else {
         res.send({"save": true, "text": "Nota Compartida"});
      } 
   });
});

app.post('/login', (req, res, next) => {
   passport.authenticate('local', (err, user) => {
      switch (req.accepts('html', 'json')) {
         case 'html':
            if (err) { return next(err); }
            if (!user) { return res.redirect('/login'); }
            req.logIn(user, function(err) {
            if (err) { return next(err); }
               return res.redirect('/profile');
            });
            break;
         case 'json':
            if (err)  { return next(err); }
            if (!user) { return res.status(401).send({"login": false}); }
            req.logIn(user, function(err) {
            if (err) { return res.status(401).send({"login": false}); }
               return res.send({"login": true, "username": user.username});
            });
            break;
         default:
            res.status(406).send();
      }
   })(req, res, next);
});

app.post('/register', (req, res, next) => {
   let db = req.body;
   if(db.username === "" || db.password === ""){
      return res.status(500).send({"save": false, "text": "Campos vacios"});
   }
   User.findOne({username: db.username}, (err, docs) => {
      if(err) {
         res.json();
      }
      if(docs) {
         res.status(500).send({"save": false, "text": "Usuario ya existe"});
      } else {
         if(db.password !== db.confirm){
               res.status(500).send({"save": false, "text": "La Confirmacion es incorrecta"});
            } else {
               let user = new User({
                  username: db.username,
                  password: db.password
               });
               user.save((err, resp) => {
                  if(err){
                     res.status(500).send({"save": false, "text": "Error"});
                  } else {
                     res.send({"save": true, "text": "Usuario Registrado"});
                  }
               });
            }
      }      
   });
  
});

app.get('/usersnote/:noteID', ensureAuth, (req, res) => {
   User.find({_id: {$ne: req.user._id}}, (err, docs) => {
      if(err) {
         res.json();
      }
      if(docs) {
         if(!docs.length){
            docs.push({noData: true});
         }
         Note.findOne({_id: req.params.noteID}, (err2, docs2) => {
            console.log(docs2);
            let shares = docs2.shares || [];
            return res.json({users: docs, shares: shares});
         });
      }else {
         res.json({});
      }
   });
   /*dbapi.notes.find((notes) => {
      res.json(notes);
   });*/
});

app.get('/shares',ensureAuth , (req, res) => {  

      Note.find({'shares.user_id': req.user._id}, (err, docs) => {
         if(err) {
            res.json();
         }
         if(docs) {
            if(!docs.length){
               docs.push({noData: true});   
            }
            res.json(docs);
         }else {
            res.json({});
         }
      });
   
});

app.get('/', (req, res) => {
   //res.sendFile(__dirname + '/index.html');
   let userName = "";
   if(req.user){
      userName = req.user.username;
   }
   res.render('index', {userName: userName});
});

app.get('/login', ensureAuth, (req, res) => {
   res.sendFile(__dirname + '/login.html');
});

app.get('/logout', (req, res) => {
  req.logout()
  res.redirect('/')
});

app.get('/welcome', ensureAuth, (req, res) => {
   res.send(`You are welcome ${req.user.username}`);
});

//middleware authenticate
function ensureAuth(req, res, next) {
   if(req.isAuthenticated()) {
      console.log("LOGIN YES");
      return next();
   }
   console.log("LOGIN NO");
   //return next();
   return res.status(401).send({"login": false});
   //res.redirect('/login');
}

let server = http.createServer(app).listen(port, () => {
   console.log(`El servidor esta levantado en el puerto ${port}`);
});